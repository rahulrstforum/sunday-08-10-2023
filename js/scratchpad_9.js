// // // // function getData() {
// // // // 	const data = axios.get('https://swapi.dev/api/planets');
// // // // 	data.then((response) => {
// // // // 		console.log(response);
// // // // 	});
// // // // }

// // // // getData();

// // // // async function greet() {
// // // // 	return 'Hello World';
// // // // }

// // // // function greet() {
// // // // 	return new Promise((resolve, reject) => {
// // // // 		resolve('Hello World');
// // // // 	});
// // // // }

// // // // async function greet() {
// // // // 	const randomNum = Math.random();
// // // // 	if (randomNum > 0.5) {
// // // // 		return 'SUCCESS';
// // // // 	} else {
// // // // 		throw 'FAILURE';
// // // // 	}
// // // // }

// // // // async function greet() {
// // // // 	throw 'Hello World';
// // // // }

// // // // function greet() {
// // // // 	return new Promise((resolve, reject) => {
// // // // 		reject('Hello World');
// // // // 	});
// // // // }

// // // // async function greet() {
// // // // 	const randomNum = Math.random();
// // // // 	if (randomNum > 0.5) {
// // // // 		return 'SUCCESS';
// // // // 	} else {
// // // // 		throw 'FAILURE';
// // // // 	}
// // // // }

// // // // function greet() {
// // // // 	return new Promise((resolve, reject) => {
// // // // 		const randomNum = Math.random();
// // // // 		if (randomNum > 0.5) {
// // // // 			resolve('SUCCESS');
// // // // 		} else {
// // // // 			reject('FAIL');
// // // // 		}
// // // // 	});
// // // // }

// // // // greet()
// // // // 	.then((data) => {
// // // // 		console.log('IN THE THEN BLOCK\n', data);
// // // // 	})
// // // // 	.catch((err) => {
// // // // 		console.log('IN THE CATCH BLOCK\n', err);
// // // // 	});

// // // // async function getData() {
// // // // 	try {
// // // // 		const response = await axios.get('https://swapi.dev/api/planets');
// // // // 		console.log(response);
// // // // 	} catch (err) {
// // // // 		console.log(err.message);
// // // // 	}
// // // // }

// // // // getData();

// // // // console.log('Hello world');

// // // // const getData = async () => {
// // // // 	const person1 = await axios.get(`https://swapi.dev/api/people/1`);
// // // // 	const home = await axios.get(person1.data.homeworld);
// // // // 	const film = await axios.get(home.data.films[0]);
// // // // 	console.log(person1.data);
// // // // 	console.log(home.data);
// // // // 	console.log(film.data);
// // // // };

// // // // getData();

// // // // const getData = async () => {
// // // // 	const person1 = await axios.get(`https://swapi.dev/api/people/1`);
// // // // 	const person2 = await axios.get(`https://swapi.dev/api/people/2`);
// // // // 	const person3 = await axios.get(`https://swapi.dev/api/people/3`);
// // // // 	console.log(person1.data);
// // // // 	console.log(person2.data);
// // // // 	console.log(person3.data);
// // // // };

// // // // getData();

// // // const getData = async () => {
// // // 	const person1Promise = axios.get(`https://swapi.dev/api/people/1`);
// // // 	const person2Promise = axios.get(`https://swapi.dev/api/people/2`);
// // // 	const person3Promise = axios.get(`https://swapi.dev/api/people/3`);

// // // 	const results = await Promise.all([
// // // 		person1Promise,
// // // 		person2Promise,
// // // 		person3Promise,
// // // 	]);
// // // 	console.log(results);

// // // 	// const person1 = await person1Promise;
// // // 	// const person2 = await person2Promise;
// // // 	// const person3 = await person3Promise;
// // // 	// console.log(person1.data);
// // // 	// console.log(person2.data);
// // // 	// console.log(person3.data);
// // // };

// // // getData();

// // function initializeDeck() {
// // 	const deck = [];
// // 	const suits = ['hearts', 'diamonds', 'spades', 'clubs'];
// // 	const values = '2,3,4,5,6,7,8,9,10,J,Q,K,A';
// // 	for (let value of values.split(',')) {
// // 		for (let suit of suits) {
// // 			deck.push({ value, suit });
// // 		}
// // 	}
// // 	return deck;
// // }

// // function drawCard(deck, drawnCards) {
// // 	const card = deck.pop();
// // 	drawnCards.push(card);
// // 	return card;
// // }

// // function drawMultiple(numCards, deck, drawnCards) {
// // 	const cards = [];
// // 	for (let i = 0; i < numCards; i++) {
// // 		cards.push(drawCard(deck, drawnCards));
// // 	}
// // 	return cards;
// // }

// // function shuffle(deck) {
// // 	// loop over array backwards
// // 	for (let i = deck.length - 1; i > 0; i--) {
// // 		// pick random index before current element
// // 		let j = Math.floor(Math.random() * (i + 1));
// // 		[deck[i], deck[j]] = [deck[j], deck[i]];
// // 	}
// // 	return deck;
// // }

// // const deck1 = initializeDeck();
// // const hand = [];

// // shuffle(deck1);

// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawMultiple(3, deck1, hand);

// // console.log(deck1);
// // console.log(hand);

// // const makeDeck = () => {
// // 	return {
// // 		deck: [],
// // 		drawnCards: [],
// // 		suits: ['hearts', 'diamonds', 'spades', 'clubs'],
// // 		values: '2,3,4,5,6,7,8,9,10,J,Q,K,A',
// // 		initializeDeck: function () {
// // 			for (let value of this.values.split(',')) {
// // 				for (let suit of this.suits) {
// // 					this.deck.push({ value, suit });
// // 				}
// // 			}
// // 			return this.deck;
// // 		},
// // 		drawCard() {
// // 			const card = this.deck.pop();
// // 			this.drawnCards.push(card);
// // 			return card;
// // 		},
// // 		drawMultiple(numCards) {
// // 			const cards = [];
// // 			for (let i = 0; i < numCards; i++) {
// // 				cards.push(this.drawCard());
// // 			}
// // 			return cards;
// // 		},
// // 		shuffle() {
// // 			const { deck } = this;
// // 			for (let i = deck.length - 1; i > 0; i--) {
// // 				let j = Math.floor(Math.random() * (i + 1));
// // 				[deck[i], deck[j]] = [deck[j], deck[i]];
// // 			}
// // 		},
// // 	};
// // };

// // // const deck1 = makeDeck();
// // // deck1.initializeDeck();
// // // deck1.shuffle();
// // // deck1.drawCard();
// // // deck1.drawCard();
// // // deck1.drawCard();
// // // deck1.drawCard();
// // // deck1.drawMultiple(3);
// // // console.log(deck1);

// // const deck1 = makeDeck();
// // const deck2 = makeDeck();

// // console.log(deck1);
// // console.log(deck2);

// // function person(firstName, lastName, age) {
// // 	return {
// // 		firstName,
// // 		lastName,
// // 		age,
// // 		greet() {
// // 			console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// // 		},
// // 	};
// // }

// // function Person(firstName, lastName, age) {
// // 	this.firstName = firstName;
// // 	this.lastName = lastName;
// // 	this.age = age;
// // }

// // Person.prototype.greet = function () {
// // 	console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// // };

// // const user1 = person('John', 'Doe', 20);
// // const user2 = person('Jane', 'Doe', 23);
// // const user3 = new Person('Jill', 'Roe', 26);
// // const user4 = new Person('askjdhajsd', 'Roe', 26);

// // // console.log(user1);
// // // console.log(user2);
// // // console.log({ firstName: 'Jack', lastName: 'Smith', age: 30, greet() {} });
// // console.log(user3);
// // console.log(user4);

// // // user1.greet();
// // // user2.greet();

// // function Person(firstName, lastName, age) {
// // 	this.firstName = firstName;
// // 	this.lastName = lastName;
// // 	this.age = age;
// // }

// // Person.prototype.greet = function () {
// // 	console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// // };

// // class Person {
// // 	constructor(firstName, lastName, age) {
// // 		this.firstName = firstName;
// // 		this.lastName = lastName;
// // 		this.age = age;
// // 	}

// // 	greet() {
// // 		console.log(`Hello, my name is ${this.firstName} ${this.lastName}`);
// // 	}
// // }

// // const user3 = new Person('Jill', 'Roe', 26);
// // const user4 = new Person('Jack', 'Smith', 30);

// // console.log(user3);
// // console.log(user4);

// class User {
// 	constructor(firstName, lastName, age) {
// 		this.firstName = firstName;
// 		this.lastName = lastName;
// 		this.age = age;
// 	}

// 	login() {
// 		console.log('Hello');
// 	}
// 	logout() {
// 		console.log('Goodbye');
// 	}
// }

// class Admin extends User {
// 	constructor(firstName, lastName, age, phone) {
// 		super(firstName, lastName, age);
// 		this.phone = phone;
// 	}
// 	createGroup() {
// 		console.log('Group was created');
// 	}
// }

// const user1 = new User('John', 'Doe', 20);
// const admin1 = new Admin('Jack', 'Smith', 30, '23123');
// console.log(user1);
// console.log(admin1);
// admin1.createGroup();
// admin1.login();
// user1.login();

// function add(a, b) {
// 	return a + b;
// }

// console.log(add(10, 20));
// console.log(add('hello', 'world'));
// console.log(add('hello', null));
