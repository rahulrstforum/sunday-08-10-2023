// console.log('Hello World!');

// console.dir(document);
// console.dir(document.all);

// const el = document.getElementById('title');
// console.log(el);

// const lis = document.getElementsByTagName('li');
// console.log(lis);

// const hellos = document.getElementsByClassName('hello');
// console.log(hellos);

// const ul = document.getElementsByTagName('ul')[0];
// const hellos = ul.getElementsByClassName('hello');
// console.log(hellos);

// const el = document.querySelectorAll('.hello');
// console.log(el);

// const h1 = document.querySelector('h1');
// console.dir(h1.innerText);
// h1.innerText = 'TEST MESSAGE';

// setInterval(function () {
// 	h1.innerText = Math.random();
// }, 1000);

// const p = document.querySelector('.special');
// console.log(p.textContent);

// const p = document.querySelector('p');
// // console.log(p);
// // p.innerHTML = 'Hello World, <b>this is something</b>.';
// console.log(p);

const a = document.querySelector('a');
// console.log(a.href);
// a.href = 'https://duckduckgo.com';
// a.id = 'impAnchor';
// a.className = 'hello';
// console.log(a.href);
// console.log(a.getAttribute('href'));
// console.log(a.id);
// console.log(a.getAttribute('id'));

// a.id = 'title';
// a.setAttribute('id', 'title');

// const li = document.querySelector('.hello');
// console.log(li.children);

// const lis = document.querySelectorAll('li');
// // console.log(li);

// for (let item of lis) {
// 	setInterval(function () {
// 		item.innerText = Math.random();
// 	}, 10);
// }

// const ul = document.querySelector('ul');
// ul.innerText = 'hello';

// const h1 = document.querySelector('h1');
// h1.style.color = 'red';
// h1.style.backgroundColor = 'yellow';

// const colors = [
// 	'red',
// 	'yellow',
// 	'green',
// 	'blue',
// 	'purple',
// 	'orange',
// 	'pink',
// 	'grey',
// 	'brown',
// 	'violet',
// ];

// const fontSizes = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// document.body.style.backgroundColor = colors[0];

// setInterval(function () {
// 	let randomColor = Math.floor(Math.random() * colors.length);
// 	let randomSize = Math.floor(Math.random() * fontSizes.length);
// 	document.body.style.backgroundColor = colors[randomColor];

// 	for (let el of document.body.children) {
// 		el.style.fontSize = `${fontSizes[randomSize]}px`;
// 	}
// }, 50);

// const listItem = document.querySelectorAll('li')[1];
// // listItem.className = 'done';
// // console.log(listItem.classList);
// // console.log(listItem.innerText);

// listItem.classList.add('done');
// listItem.classList.remove('hello');
// listItem.classList.toggle('world');

// const root = document.querySelector('#root');
// const h1 = document.createElement('h1');
// h1.innerText = 'Hello World Using the DOM';
// h1.style.color = 'red';
// h1.className = 'title';

// root.appendChild(h1);

// const li = document.createElement('li');
// li.innerText = 'Hello world';

// const secondLi = document.querySelectorAll('li')[1];

// const ul = document.querySelector('ul');
// // ul.appendChild(li);

// ul.insertBefore(li, secondLi);

// const p = document.querySelector('p');

// const b = document.createElement('b');
// b.innerText = 'hello world';
// p.insertAdjacentElement('afterend', b);

// const p = document.querySelector('p');

// const b = document.createElement('b');
// b.innerText = 'hello world';
// const i = document.createElement('i');
// i.innerText = 'good bye world';

// p.prepend(b, i);

// p.remove();

// const button = document.querySelector('button');

// button.onclick = function () {
// 	console.log('Hello World');
// };

// button.onclick = function () {
// 	console.log('Goodbye World');
// };

// button.addEventListener('click', function () {
// 	console.log('Hello World');
// });
// button.addEventListener('click', function () {
// 	console.log('Goodbye World');
// });

const root = document.querySelector('#root');

const textInput = document.createElement('input');
textInput.type = 'text';

const ul = document.createElement('ul');
const title = document.createElement('h3');
title.innerText = 'My List';

const button = document.createElement('button');
button.innerText = 'Add';
button.addEventListener('click', function () {
	const li = document.createElement('li');

	const delBtn = document.createElement('button');
	delBtn.innerText = 'Delete';
	delBtn.addEventListener('click', function () {
		li.remove();
	});

	li.innerText = textInput.value;
	li.addEventListener('click', function () {
		li.classList.toggle('done');
	});
	li.append(delBtn);
	ul.append(li);
	textInput.value = '';
});

root.append(textInput, button, title, ul);

// window.addEventListener('scroll', function () {
// 	console.log(Math.random());
// });

// const btn = document.querySelector('button');
// btn.style.position = 'relative';

// btn.addEventListener('mouseover', function () {
// 	const height = Math.floor(Math.random() * window.innerHeight);
// 	const width = Math.floor(Math.random() * window.innerWidth);
// 	btn.style.left = `${width}px`;
// 	btn.style.top = `${height}px`;
// });

// btn.addEventListener('click', function () {
// 	btn.innerText = 'You Won!';
// 	document.body.style.backgroundColor = 'green';
// });

// const btn = document.querySelector('button');

// btn.addEventListener('click', function (event) {
// 	console.log(event);
// });

// const form = document.querySelector('form');
// const ul = document.querySelector('ul');

// form.addEventListener('submit', function (event) {
// 	event.preventDefault();

// 	const li = document.createElement('li');
// 	li.innerText = event.target[0].value;
// 	ul.append(li);

// 	event.target[0].value = '';
// });
