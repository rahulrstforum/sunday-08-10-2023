// // // function multiply(x, y) {
// // // 	return x * y;
// // // }

// // // function square(num) {
// // // 	return multiply(num, num);
// // // }

// // // function rightTriangle(x, y, z) {
// // // 	return square(x) + square(y) === square(z);
// // // }

// // // console.log(rightTriangle(1, 2, 3));

// // // console.log('The first log');
// // // alert('This is something!');
// // // console.log('The second log');

// // // console.log('The first log');
// // // setTimeout(function () {
// // // 	console.log('Hello World!');
// // // }, 5000);
// // // console.log('The second log');

// // const btn = document.querySelector('button');

// // setTimeout(function () {
// // 	btn.style.transform = 'translateX(100px)';
// // 	setTimeout(function () {
// // 		btn.style.transform = 'translateX(200px)';
// // 		setTimeout(function () {
// // 			btn.style.transform = 'translateX(300px)';
// // 			setTimeout(function () {
// // 				btn.style.transform = 'translateX(400px)';
// // 				setTimeout(function () {
// // 					btn.style.transform = 'translateX(500px)';
// // 					setTimeout(function () {
// // 						btn.style.transform = 'translateX(600px)';
// // 					}, 1000);
// // 				}, 1000);
// // 			}, 1000);
// // 		}, 1000);
// // 	}, 1000);
// // }, 1000);

// // const willGetAPlayStation = new Promise((resolve, reject) => {
// // 	const randomNum = Math.random();

// // 	if (randomNum > 0.5) {
// // 		resolve();
// // 	} else {
// // 		reject();
// // 	}
// // });

// // willGetAPlayStation
// // 	.then(() => {
// // 		console.log('Thank you uncle!!!');
// // 	})
// // 	.catch(() => {
// // 		console.log('########### you uncle!!!');
// // 	});

// // console.log('Hello World!');

// // function makePSPromise() {
// // 	return new Promise((resolve, reject) => {
// // 		setTimeout(() => {
// // 			const randomNum = Math.random();

// // 			if (randomNum > 0.5) {
// // 				resolve();
// // 			} else {
// // 				reject();
// // 			}
// // 		}, 3000);
// // 	});
// // }

// // const result = makePSPromise();

// // result
// // 	.then(function () {
// // 		console.log('Promise was fulfilled!');
// // 	})
// // 	.catch(function () {
// // 		console.log('REJECTED!');
// // 	});

// // console.log('Hello World!');

// function fakeRequest(url) {
// 	return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			const pages = {
// 				'/users': [
// 					{ id: 1, username: 'john' },
// 					{ id: 2, username: 'jane' },
// 				],
// 				'/about': 'This is the about page',
// 				'/users/1': {
// 					id: 1,
// 					username: 'johndoe',
// 					topPostId: 53231,
// 					city: 'mumbai',
// 				},
// 				'/users/5': {
// 					id: 1,
// 					username: 'janedoe',
// 					topPostId: 32443,
// 					city: 'pune',
// 				},
// 				'/posts/53231': {
// 					id: 1,
// 					title: 'Really amazing post',
// 					slug: 'really-amazing-post',
// 				},
// 			};

// 			const data = pages[url];

// 			if (data) {
// 				resolve({ status: 200, data });
// 			} else {
// 				reject({ status: 404 });
// 			}
// 		}, 2000);
// 	});
// }

// // fakeRequest('/users')
// // 	.then((response) => {
// // 		const userId = response.data[0].id;

// // 		fakeRequest(`/users/${userId}`)
// // 			.then((response) => {
// // 				const postId = response.data.topPostId;

// // 				fakeRequest(`/posts/${postId}`)
// // 					.then((response) => {
// // 						console.log(response);
// // 					})
// // 					.catch((err) => {
// // 						console.log(err);
// // 					});
// // 			})
// // 			.catch((err) => {
// // 				console.log(err);
// // 			});
// // 	})
// // 	.catch((err) => {
// // 		console.log(err);
// // 	});

// fakeRequest('/users')
// 	.then((response) => {
// 		const userId = response.data[0].id;
// 		return fakeRequest(`/uasdasdasdsers/${userId}`);
// 	})
// 	.then((response) => {
// 		const postId = response.data.topPostId;
// 		return fakeRequest(`/posts/${postId}`);
// 	})
// 	.then((response) => {
// 		console.log(response);
// 	})
// 	.catch((err) => {
// 		console.log('CATCH CODE', err);
// 	});

// fetch('https://pokeapi.co/api/v2/pokemon')
// 	.then((response) => {
// 		response.json().then((data) => {
// 			console.log('THE DATA', data);
// 		});
// 	})
// 	.catch((err) => {
// 		console.log('IN THE CATCH BLOCK', err);
// 	});

// fetch('https://pokeapi.co/api/v2/poksdlkjSNDfkjlsdbflksjdfemon')
// 	.then((response) => {
// 		if (!response.ok) {
// 			throw new Error();
// 		}
// 		response.json().then((data) => {
// 			console.log(data);
// 		});
// 	})
// 	.catch((err) => {
// 		console.log('IN THE CATCH BLOCK', err);
// 	});

const root = document.querySelector('#root');

const grid = document.createElement('div');
grid.style.display = 'grid';
grid.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr';
grid.style.gap = '30px';

axios
	.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
	.then((response) => {
		for (let pokemon of response.data.results) {
			axios.get(pokemon.url).then((response) => {
				let pokeCard = document.createElement('article');
				pokeCard.style.display = 'flex';
				pokeCard.style.justifyContent = 'center';
				pokeCard.style.alignItems = 'center';
				pokeCard.style.flexDirection = 'column';
				pokeCard.style.gap = '10px';

				const pokeImg = document.createElement('img');
				pokeImg.style.width = '100%';

				pokeImg.src =
					response.data.sprites.other['official-artwork'].front_default;
				const pokeName = document.createElement('h4');

				pokeName.innerText = pokemon.name;
				pokeName.style.fontSize = '30px';
				pokeName.style.textTransform = 'capitalize';
				pokeCard.append(pokeImg, pokeName);
				grid.append(pokeCard);
			});
		}
	})
	.catch((err) => {
		console.log('IN THE CATCH BLOCK', err);
	});

root.append(grid);
