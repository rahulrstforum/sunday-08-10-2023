// let loggedInUser = null;

// if (loggedInUser) {
// 	console.log('Welcome');
// } else {
// 	console.log('Please login to continue');
// }

// let age = 80;

// if (age >= 18 && age < 21) {
// 	console.log("You can enter, but you can't drink");
// } else if (age >= 21 && age < 65) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 65) {
// 	console.log('Drinks are free');
// } else {
// 	console.log("You aren't allowed");
// }

// let day = 2;

// switch (day) {
// 	case 1:
// 		console.log('Mon');
// 		break;
// 	case 2:
// 	case 3:
// 	case 4:
// 		console.log('Thu');
// 		break;
// 	case 5:
// 		console.log('Fri');
// 		break;
// 	case 6:
// 		console.log('Sat');
// 		break;
// 	case 7:
// 		console.log('Sun');
// 		break;
// 	default:
// 		console.log('Invalid day code');
// }

// // if (day === 1) {
// // 	console.log('Sun');
// // } else if (day === 2) {
// // 	console.log('Mon');
// // } else if (day === 3) {
// // 	console.log('Tue');
// // } else {
// // 	console.log('Invalid day code');
// // }

// let currentStatus = 'idle';
// let color =
// 	currentStatus === 'online'
// 		? 'green'
// 		: currentStatus === 'offline'
// 		? 'red'
// 		: 'yellow';

// let color;

// if (currentStatus === 'online') {
// 	color = 'green';
// } else if (currentStatus === 'offline') {
// 	color = 'red';
// } else {
// 	color = 'yellow';
// }

// console.log(color);

const product1 = [
	'iPhone 15',
	'Apple',
	'Some description...',
	500,
	100000,
	true,
	'https://',
];

// const product2 = {
// 	name: 'iPhone 15',
// 	brand: 'Apple',
// 	description: 'Some description...',
// 	inStock: 500,
// 	price: 100000,
// 	discounted: true,
// 	imageUrl: 'https://...',
// 	1: null,
// 	'hello world': false,
// };

// console.log(product1[4]);
// // console.log(product2['price']);
// console.log(product2.price);
// console.log(product2[1]);
// console.log(product2['hello world']);

const pallette = {
	red: '#eb4d4b',
	yellow: '#f9ca24',
	blue: '#30336b',
};

let color = 'yellow';

console.log(pallette[color]);
