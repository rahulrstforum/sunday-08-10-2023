// // const profile = {
// // 	firstName: 'John',
// // 	lastName: 'Doe',
// // 	age: 20,
// // };

// // profile = true;

// // let name = 'john';
// // name = 'john doe';

// // const profile = {
// // 	firstName: 'John',
// // 	lastName: 'Doe',
// // 	age: 20,
// // };

// // profile.hello = 'world';

// // console.log(profile);

// const student = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	frameworks: ['ReactJS', 'Express.js'],
// 	levels: {
// 		backend: 50,
// 		frontend: 45,
// 		data: 5,
// 	},
// };

// console.log(student.levels.frontend);

// for (let count = 0; count < 10; count++) {
// 	console.log(count, 'Hello World');
// }

// for (let count = 10; count >= 0; count++) {
// 	console.log(count);
// }

// const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// for (let i = 0; i < nums.length; i++) {
// 	console.log(nums[i]);
// }

// const movies = [
// 	{ movieName: 'Inception', rating: 3.8 },
// 	{ movieName: 'Avengers', rating: 3.4 },
// 	{ movieName: 'Iron Man', rating: 2.9 },
// ];

// for (let i = 0; i < movies.length; i++) {
// 	let movie = movies[i];
// 	console.log(`${movie.movieName} has a rating of ${movie.rating}`);
// }

// const word = 'Hello World';

// for (let i = 0; i < word.length; i++) {
// 	console.log(word[i]);
// }

// const word = 'Hello';

// let reveresedWord = '';
// for (let i = word.length - 1; i >= 0; i--) {
// 	reveresedWord += word[i];
// }

// console.log(reveresedWord);

// for (let i = 0; i < 5; i++) {
// 	console.log('OUTER LOOP -', i);

// 	for (let j = 0; j < 5; j++) {
// 		console.log('	INNER LOOP -', j);
// 	}
// }

// const gameBoard = [
// 	[4, 64, 8, 4],
// 	[128, 32, 4, 16],
// 	[16, 4, 4, 32],
// 	[2, 16, 16, 2],
// ];

// for (let i = 0; i < gameBoard.length; i++) {
// 	// console.log(gameBoard[i]);
// 	for (let j = 0; j < gameBoard[i].length; j++) {
// 		console.log(gameBoard[i][j]);
// 	}
// }

// for (let i = 0; i < 10; i++) {
// 	console.log(i);
// }

// let i = 0;

// while (i < 10) {
// 	console.log(i);
// 	i++;
// }

// const target = Math.floor(Math.random() * 100) + 1; // 90
// let guess = Math.floor(Math.random() * 100) + 1; // 12

// while (guess !== target) {
// 	if (guess === 13) {
// 		console.log('13 is an unlucky number');
// 		break;
// 	}
// 	console.log(`Target: ${target} | Guess: ${guess}`);
// 	guess = Math.floor(Math.random() * 100) + 1; // 90
// }

// console.log('Final Result:');
// console.log(`Target: ${target} | Guess: ${guess}`);

// const target = Math.floor(Math.random() * 100) + 1; // 90
// let guess = Math.floor(Math.random() * 100) + 1; // 12

// while (true) {
// 	if (guess === target) {
// 		break;
// 	}
// 	console.log(`Target: ${target} | Guess: ${guess}`);
// 	guess = Math.floor(Math.random() * 100) + 1; // 90
// }

// console.log('Final Result:');
// console.log(`Target: ${target} | Guess: ${guess}`);

// const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// // for (let i = 0; i < nums.length; i++) {
// // 	console.log(nums[i]);
// // }

// for (let num of nums) {
// 	console.log(num);
// }

// for (let char of 'hello world') {
// 	console.log(char);
// }

// const matrix = [
// 	[1, 4, 7],
// 	[9, 7, 2],
// 	[9, 4, 6],
// ];

// for (let row of matrix) {
// 	for (let col of row) {
// 		console.log(col);
// 	}
// }

// const cats = ['fashion', 'mobiles', 'books'];
// const prods = ['tshirt', 'samsung', '1984'];

// for (let i = 0; i < cats.length; i++) {
// 	console.log(cats[i], prods[i]);
// }

const productPrices = {
	Apple: 80000,
	OnePlus: 50000,
	Samsung: 90000,
};

// for (let item of Object.keys(productPrices)) {
// 	console.log(item, productPrices[item]);
// // }

// for (let key of Object.keys(productPrices)) {
// 	console.log(key, productPrices[key]);
// }

// for (let key in productPrices) {
// 	console.log(key, productPrices[key]);
// }

// function greet() {
// 	console.log('Hello World');
// 	console.log('Hello World');
// 	console.log('Hello World');
// }

// greet();

// function flipCoin() {
// 	const num = Math.random();

// 	if (num > 0.5) {
// 		console.log('HEADS');
// 	} else {
// 		console.log('TAILS');
// 	}
// }

// flipCoin();

// function rollDie() {
// 	let roll = Math.floor(Math.random() * 6) + 1;
// 	console.log(`Rolled: ${roll}`);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(2);

function greet(firstName, lastName) {
	console.log(`Hello ${firstName} ${lastName}`);
}

greet('john', 'Doe');
greet('Jane', 'Roe');
