// function greet(message, name) {
// 	console.log(`${message}, ${name}`);
// }

// greet('hello', 'jane');

// let message = 'Hello World';

// let upper_message = message.toUpperCase();

// console.log('HELLO WORLD');

// function greet() {
// 	return 'Hello World';
// }

// console.log(greet());

// let greetResult = greet();

// console.log(greetResult);

// function isNumber(num) {
// 	if (typeof num === 'number') {
// 		console.log('Some message');
// 		return true;
// 	} else {
// 		return false;
// 	}
// }

// console.log(isNumber(123));

// let message = 'Hello World';

// function greet() {
// 	let message = 'Goodbye World!';
// 	console.log(message);
// }

// greet();
// console.log(message);

// if (true) {
// 	let message = 'Hello';
// 	console.log(message);
// }

// console.log(message);

// for (var i = 0; i < 10; i++) {
// 	console.log(i);
// }

// console.log(i);

// function outer() {
// 	function inner() {
// 		let message = 'Hello';
// 		console.log(message);
// 	}
// 	inner();
// 	console.log(message);
// }

// outer();

// function greet() {
// 	console.log('Hello');
// }

// let test = greet;

// test();

// const hello = function () {
// 	console.log('Hello');
// };

// hello();

// function math(a, b, fn) {
// 	return fn(a, b);
// }

// function add(a, b) {
// 	return a + b;
// }

// function sub(a, b) {
// 	return a - b;
// }

// // console.log(add(10, 20));

// console.log(
// 	math(10, 20, function (a, b) {
// 		return a * b;
// 	})
// );

// (function () {
// 	console.log('hello');
// })();

// const math = [
// 	function (a, b) {
// 		return a + b;
// 	},
// 	function (a, b) {
// 		return a - b;
// 	},
// 	function (a, b) {
// 		return a * b;
// 	},
// 	function (a, b) {
// 		return a / b;
// 	},
// 	3.14,
// ];

// console.log(math[4]);

// const math = {
// 	add: function (a, b) {
// 		return a + b;
// 	},
// 	sub: function (a, b) {
// 		return a - b;
// 	},
// 	mul: function (a, b) {
// 		return a * b;
// 	},
// 	div: function (a, b) {
// 		return a / b;
// 	},
// 	PI: 3.14,
// };

// console.log(math.add(10, 20));

// function math(a, b, fn) {
// 	return fn(a, b);
// }

// math(10, 20, function (a, b) {
// 	return a + b;
// });

// Example 2
// function repeat(func, num) {
// 	for (let i = 0; i < num; i++) {
// 		func();
// 	}
// }

// function sayHello() {
// 	console.log('Hello World!');
// }
// function sayGoodbye() {
// 	console.log('Bye World!');
// }

// repeat(sayHello, 10);
// repeat(sayGoodbye, 5);

// function randomPick(f1, f2) {
// 	let randNum = Math.random();
// 	if (randNum < 0.5) {
// 		f1();
// 	} else {
// 		f2();
// 	}
// }

// randomPick(
// 	function () {
// 		console.log('Hello');
// 	},
// 	function () {
// 		console.log('Bye');
// 	}
// );

// function raiseBy(num) {
// 	return function (x) {
// 		return x ** num;
// 	};
// }

// // const square = raiseBy(2);
// // const cube = raiseBy(3);
// // const hypercube = raiseBy(4);

// // const cube = raiseBy(3);
// const square = raiseBy(2);

// console.log(square(2));

// function isBetween(x, y) {
// 	return function (num) {
// 		return num >= x && num <= y;
// 	};
// }

// const isUnderAge = isBetween(0, 18);
// const canEnterButNotDrink = isBetween(18, 21);
// const canDrink = isBetween(21, 65);
// const isSenior = isBetween(65, 100);

// console.log(test(40));

// function math(a, b, fn) {
// 	return fn(a, b);
// }

// function add(a, b) {
// 	return a + b;
// }

// math(10, 20, add);

// setInterval(function () {
// 	console.log(Math.random());
// }, 5);

// function greet() {
// 	console.log('hello world');
// }

// greet();

// let hello = 'hello world';
// console.log(hello);

// const nums = [1, 2, 3, 34, 45];

// // for (let num of nums) {
// // 	console.log(num);
// // }

// nums.forEach(function (n) {
// 	console.log('hello', n);
// });

// const movies = [
// 	{
// 		title: 'Avengers',
// 		rating: 4.1,
// 	},
// 	{
// 		title: 'Dr. Strange',
// 		rating: 3.9,
// 	},
// 	{
// 		title: 'Tenet',
// 		rating: 4.3,
// 	},
// 	{
// 		title: 'Joker',
// 		rating: 4.7,
// 	},
// ];

// movies.forEach(function (movie, i) {
// 	console.log(`${i}: ${movie.title} has a rating of ${movie.rating}`);
// });

// const names = ['john', 'jack', 'jane', 'james'];

// const upperNames = names.map(function (name) {
// 	return name.toUpperCase();
// });

// const upperNames = [];

// for (let name of names) {
// 	upperNames.push(name.toUpperCase());
// }

// console.log(upperNames);

// const nums = [2, 3, 4, 7, 6, 8, 13, 10, 19, 12, 14, 22, 21, 16];

// const doubles = nums.map(function (num) {
// 	return {
// 		num: num,
// 		isEven: num % 2 === 0,
// 	};
// });

// console.log(doubles);

// const square = n => n ** 2;

// console.log(square(10));

// const names = ['john', 'jack', 'jane', 'james'];

// const upperNames = names.map((name) => name.toUpperCase());
// console.log(upperNames);

// const nums = [1, 2, 3, 4, 5, 6];

// // using regular function expressions
// const doubles1 = nums.map(function (n) {
// 	return n * 2;
// });

// // using arrow functions
// const doubles2 = nums.map((n) => {
// 	return n * 2;
// });

// // using arrow function's one-liner implicit return
// const doubles3 = nums.map((n) => n * 2);
// const parityList = nums.map((n) => (n % 2 === 0 ? 'Even' : 'Odd'));

// let movies = ['The Terminator', 'The Avengers', 'Jurassic Park', 'Titanic'];

// const result = movies.find((movie) => {
// 	return movie[0] === 'J';
// });

// console.log(result);

// const books = [
// 	{
// 		title: 'The Shining',
// 		author: 'Stephen King',
// 		rating: 4.1,
// 	},
// 	{
// 		title: 'Sacred Games',
// 		author: 'Vikram Chandra',
// 		rating: 4.5,
// 	},
// 	{
// 		title: '1984',
// 		author: 'George Orwell',
// 		rating: 4.9,
// 	},
// 	{
// 		title: 'The Alchemist',
// 		author: 'Paulo Coelho',
// 		rating: 3.5,
// 	},
// 	{
// 		title: 'The Great Gatsby',
// 		author: 'F. Scott Fitzgerald',
// 		rating: 3.8,
// 	},
// ];

// const result = books.filter((book) => book.rating > 4.0);
// console.log(result);

// const names = ['jack', 'james', 'john', 'jane', 'josh', 'jrad'];

// const result = names.some((name) => name[0] !== 'j');

// console.log(result);

// const nums = [12, 8, 234, -2, 73, 4, 0];

// const result = nums.sort((a, b) => a - b);
// console.log(result);

// const nums = [1, 2, 3, 4, 5];

// const result = nums.reduce((acc, currVal) => acc * currVal);
// console.log(result);

// acc   currVal
// 1     2
// 3     3
// 6     4
// 10    5
// 15

// let nums = [21, 221, 2, 1, 34, 123, 4342, 56, 4];

// const result = nums.reduce((acc, currVal) => {
// 	if (currVal > acc) {
// 		return currVal;
// 	}
// 	return acc;
// });
// console.log(result);

// acc   currVal
// 21			221
// 221		2
// 221 		1
// 221		34
// 221    123
// 221		4342
// 4342		56
// 4342   4
// 4342

const nums = [1, 2, 3, 4, 5];

const result = nums.reduce((acc, currVal) => acc + currVal, 100);
console.log(result);
