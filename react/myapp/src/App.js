// import Hello from './components/Hello';
// import Form from './components/Form';
import Counter from './components/Counter';

const App = () => {
	return (
		<div>
			<h1>My App</h1>
			{/* <Hello /> */}
			{/* <Form /> */}
			<Counter />
			<Counter />
		</div>
	);
};

export default App;
