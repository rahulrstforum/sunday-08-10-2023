import React from 'react';

const Form = () => {
	const usernameRef = React.useRef();
	const passwordRef = React.useRef();

	const [error, setError] = React.useState(null);

	const [username, setUsername] = React.useState('');
	const [password, setPassword] = React.useState('');

	// const handleUsernameCheck = (e) => {
	// 	if (e.target.value !== e.target.value.toLowerCase()) {
	// 		setError('Username must not contain capital letters');
	// 	} else {
	// 		setError(null);
	// 	}
	// };

	const handleSubmit = (e) => {
		e.preventDefault();
		// console.log(e.target[0].value);
		// console.log(e.target[1].value);
		// const { username, password } = e.target.elements;
		// console.log(username.value);
		// console.log(password.value);

		console.log(usernameRef.current.value);
		console.log(passwordRef.current.value);
	};

	return (
		<div>
			<h2>My Form</h2>

			<form onSubmit={handleSubmit}>
				<div>
					<label htmlFor='username'>Username</label>
					<br />
					<input
						type='text'
						id='username'
						value={username}
						onChange={(e) => setUsername(e.target.value.toLowerCase())}
					/>
					<p
						style={{ margin: 0, padding: 0, color: 'red', fontWeight: 'bold' }}>
						{error}
					</p>
				</div>
				<div>
					<label htmlFor='password'>Password</label>
					<br />
					<input
						type='text'
						id='password'
						ref={passwordRef}
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
				</div>
				<br />
				<button type='submit' disabled={Boolean(error)}>
					Login
				</button>
			</form>
		</div>
	);
};

export default Form;
