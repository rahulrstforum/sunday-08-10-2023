import axios from 'axios';
import React from 'react';

const Counter = (props) => {
	const [count, setCount] = React.useState(0);
	const [pokemon, setPokemon] = React.useState([]);

	React.useEffect(() => {
		async function fetchData() {
			const response = await axios.get(`https://pokeapi.co/api/v2/pokemon`);
			// console.log(response.data.results);
			setPokemon(response.data.results);
		}

		fetchData();
	}, [count]);

	return (
		<div>
			<h1>Count: {count}</h1>
			<button onClick={() => setCount(count + 1)}>Increase</button>
			<button onClick={() => setCount(count - 1)}>Decrease</button>

			{/* <h1>Pokemon</h1>
			<ul>
				{pokemon.map((p) => (
					<li key={p.name}>{p.name}</li>
				))}
			</ul> */}
		</div>
	);
};

export default Counter;
